# Índice de repositorios

Este repositorio es un índice de otros repositorios.

## Bootcamps de Keepcoding

### DevOps I

* [Curso de Python](https://gitlab.com/aureliopn/00_kc_python).
* [Administración de sistemas](https://gitlab.com/aureliopn/02_sysadmin).
* [Migración a la nube](https://gitlab.com/aureliopn/03_migranube).
* [Contenedores](https://gitlab.com/aureliopn/04_contenedores).
* [Calidad](https://gitlab.com/aureliopn/05_calidad).
* [Ciclo de vida de un desarrollo CI/CD](https://gitlab.com/aureliopn/06_ciclodesa).
  * [Integración contínua](https://gitlab.com/aureliopn/06_practica-ci).
* [Liberando productos](https://gitlab.com/aureliopn/07_libeproduc).
* [Proyecto final](https://gitlab.com/aureliopn/08_proyecto-final).
  * [Netcore Counter](https://gitlab.com/aureliopn/netcore-counter).

### Cybersecurity III

* [Introducción a la ciberseguridad](https://gitlab.com/aureliopn/01_cybersecurity_introduction).
* [Criptografía](https://gitlab.com/aureliopn/02_cybersecurity_cryptography).
* [Recopilación de información](https://gitlab.com/aureliopn/03_cybersecurity_information_gathering) (privado).
* [Pentesting](https://gitlab.com/aureliopn/04_cybersecurity_pentesting).
* [Blue Team](https://gitlab.com/aureliopn/05_cybersecurity_blue_team).
* [Machine Learning](https://gitlab.com/aureliopn/06_cybersecurity_machine_learning).
* [Análisis de malware](https://gitlab.com/aureliopn/07_cybersecurity_analisis_malware).
* [Digital Forensics and Incident Response](https://gitlab.com/aureliopn/08_cybersecurity_dfir).
* [Red Team](https://gitlab.com/aureliopn/09_cybersecurity_red_team) (privado).
* [Proyecto final](https://gitlab.com/aureliopn/10_cybersecurity_proyecto_final).

## Aplicaciones

* [Cybertools](https://gitlab.com/aureliopn/cybertools).
* [Timelapse Pi](https://gitlab.com/aureliopn/timelapse-pi).
